#include <iostream>

bool isPrime(int num) {
  if (num < 2)
    return false;
  int i = 2;
  while (i++ < num / 2) {
    if(num%i == 0)
      return false;
  }
  return true;
}

int main(int argc, char const *argv[]) {
  printf("%d --> %d\n", 1, isPrime(1));
  printf("%d --> %d\n", 3, isPrime(3));
  printf("%d --> %d\n", 5, isPrime(5));
  printf("%d --> %d\n", 6, isPrime(6));
  printf("%d --> %d\n", 7, isPrime(7));
  printf("%d --> %d\n", 36, isPrime(36));
  printf("%d --> %d\n", 37, isPrime(37));
  return 0;
}
