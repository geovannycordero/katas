# include <string>
#include <iostream>

bool isVowel(char c) {
  return (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u');
}

std::string disemvowel(std::string str) {
  int i = 0;
  while(i < str.length()-1){
    if(isVowel(tolower(str[i])))
      str.erase(i, 1);
    else
      ++i;
  }
  return str;
}

int main(int argc, char const *argv[]) {
  std::cout << "This lololol website isn't rad for losers LOL! var radians rad -> " << disemvowel("This lololol website isn't rad for losers LOL! var radians rad") << std::endl;
  printf("The End!\n");
  return 0;
}

/*
Trolls are attacking your comment section!

A common way to deal with this situation is to remove all of the vowels from the trolls' comments, neutralizing the threat.

Your task is to write a function that takes a string and return a new string with all vowels removed.

For example, the string "This website is for losers LOL!" would become "Ths wbst s fr lsrs LL!".

Note: for this kata y isn't considered a vowel.

Best upvoted solution:

std::string disemvowel(std::string str)
{
  std::regex vowels("[aeiouAEIOU]");
  return std::regex_replace(str, vowels, "");
}

*/
