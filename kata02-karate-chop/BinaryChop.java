public class BinaryChop {
  public int chopOne(int target, int[] sortedArray) {
    int pivot, start, end;

    start = 0;
    end = sortedArray.length - 1;
    while(start < end) {
      pivot = (start + end) / 2;

      if(sortedArray[pivot] < target) {
        start = pivot + 1;
      } else if(sortedArray[pivot] > target) {
        end = pivot - 1;
      } else {
        return pivot;
      }
    }
    return -1;
  }

  private int chopRec(int start, int end, int target, int[] sortedArray){
    int pivot;
    if(start < end){
      pivot = (start + end) / 2;
      if(sortedArray[pivot] < target) {
        return chopRec(pivot + 1, end, target, sortedArray);
      } else if(sortedArray[pivot] > target) {
        return chopRec(start, pivot - 1, target, sortedArray);
      } else {
        return pivot;
      }
    }
    return -1;
  }

  public int chop(int target, int[] sortedArray) {
    int start = 0, end = sortedArray.length - 1;
    return chopRec(start, end, target, sortedArray);
  }
}
