import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TestJunit {
  BinaryChop binaryChop = new BinaryChop();

  @Test
  public void testChop(){
    int[] array = {};
    assertEquals(-1, binaryChop.chop(3, array));
    
    int[] array = {1};
    assertEquals(-1, binaryChop.chop(3, array));
    assertEquals(0,  binaryChop.chop(1, array));

    int[] array = {1, 3, 5};
    assertEquals(0,  binaryChop.chop(1, array));
    assertEquals(1,  binaryChop.chop(3, array));
    assertEquals(2,  binaryChop.chop(5, array));
    assertEquals(-1, binaryChop.chop(0, array));
    assertEquals(-1, binaryChop.chop(2, array));
    assertEquals(-1, binaryChop.chop(4, array));
    assertEquals(-1, binaryChop.chop(6, array));

    int[] array = {1, 3, 5, 7};
    assertEquals(0,  binaryChop.chop(1, array));
    assertEquals(1,  binaryChop.chop(3, array));
    assertEquals(2,  binaryChop.chop(5, array));
    assertEquals(3,  binaryChop.chop(7, array));
    assertEquals(-1, binaryChop.chop(0, array));
    assertEquals(-1, binaryChop.chop(2, array));
    assertEquals(-1, binaryChop.chop(4, array));
    assertEquals(-1, binaryChop.chop(6, array));
    assertEquals(-1, binaryChop.chop(8, array));
  }
}
