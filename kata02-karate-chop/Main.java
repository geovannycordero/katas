public class Main {
  public static void main(String[] args) {
    int[] array = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

    BinaryChop bn = new BinaryChop();

    System.out.println(bn.chop(1, array));
    System.out.println(bn.chop(7, array));
    System.out.println(bn.chop(2, array));
    System.out.println(bn.chop(4, array));
    System.out.println(bn.chop(10, array));

    System.out.println("This is the main file of the kata02");
  }
}
